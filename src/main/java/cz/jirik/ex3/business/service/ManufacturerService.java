package cz.jirik.ex3.business.service;

import cz.jirik.ex3.entity.Manufacturer;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class ManufacturerService {
    
    @PersistenceContext 
    private EntityManager em; 
    
    
    public List<Manufacturer> findManufacturer() {
        TypedQuery<Manufacturer> query = em. createNamedQuery(Manufacturer.GET_ALL, Manufacturer.class);
        return query.getResultList();
    }
}
