package cz.jirik.ex3;

import cz.jirik.ex3.page.ManufacturersPage;
import org.apache.wicket.Page;
import org.apache.wicket.cdi.CdiConfiguration;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.protocol.https.HttpsConfig;
import org.apache.wicket.protocol.https.HttpsMapper;

public class Ex3WicketApp extends WebApplication {

    @Override
    public void init() {
        super.init();
        configureSSLPortMapper();
        new CdiConfiguration().configure(this);
    }

    @Override
    public Class<? extends Page> getHomePage() {
        return ManufacturersPage.class;
    }

    private void configureSSLPortMapper() {
        setRootRequestMapper(new HttpsMapper(getRootRequestMapper(), new HttpsConfig(8080, 8181)));
    }
}
