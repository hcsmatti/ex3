package cz.jirik.ex3.model;


import cz.jirik.ex3.entity.Manufacturer;
import cz.jirik.ex3.page.ManufacturerController;
import java.util.List;
import javax.inject.Inject;
import org.apache.wicket.cdi.NonContextual;
import org.apache.wicket.model.util.ListModel;

public class ManufacturerModel extends ListModel<Manufacturer> {

    @Inject
    private ManufacturerController manufacturerController;
    
    public ManufacturerModel() {
        NonContextual.of(ManufacturerModel.class).inject(this);
    }
    
    @Override
    public List<Manufacturer> getObject() {
        return manufacturerController.findManufacturers();
    }
    
    
}
