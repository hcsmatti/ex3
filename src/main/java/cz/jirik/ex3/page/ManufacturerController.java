package cz.jirik.ex3.page;

import cz.jirik.ex3.business.service.ManufacturerService;
import cz.jirik.ex3.entity.Manufacturer;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

@RequestScoped
public class ManufacturerController {
    
    @Inject
    private ManufacturerService customerService;
    
    public List<Manufacturer> findManufacturers() {
        return customerService.findManufacturer();
    }
}
