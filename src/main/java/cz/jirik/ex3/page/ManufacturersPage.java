package cz.jirik.ex3.page;

import cz.jirik.ex3.entity.Manufacturer;
import cz.jirik.ex3.model.ManufacturerModel;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class ManufacturersPage extends WebPage {
    
    private ManufacturerModel manufacturerModel;
    
    public ManufacturersPage(PageParameters parameters) {
        super(parameters);
    }

    @Override
    protected void onInitialize() {
        initModel();
        add(new ListView<Manufacturer>("manufacturersMarkupId", manufacturerModel){
            @Override
            protected void populateItem(ListItem<Manufacturer> li) {
//                li.add(new Label("actionMuId", "akce")); // tady muze byt odkaz na detail, nebo jen zvyrazneni daneho prvku
                li.add(new Label("contactid", new PropertyModel(li.getModel(), "manufacturerId")));
                li.add(new Label("name", new PropertyModel(li.getModel(), "name")));
                li.add(new Label("city", new PropertyModel(li.getModel(), "city")));
                li.add(new Label("state", new PropertyModel(li.getModel(), "state")));
                li.add(new Label("email", new PropertyModel(li.getModel(), "email")));
            }            
        });
        
        super.onInitialize();
    }

    @Override
    protected IModel<?> initModel() {
        manufacturerModel = new ManufacturerModel();
        return super.initModel();        
    }

    @Override
    protected void onDetach() {
        manufacturerModel.detach();
        super.onDetach();
    }
    
    
    
}
